﻿#include <iostream>
#include <string>

int main() {
    // Создаем и инициализируем переменную типа std::string
    std::string myString = "Hello, World!";

    // Выводим саму строковую переменную
    std::cout << "String: " << myString << std::endl;

    // Выводим длину строки
    std::cout << "Length: " << myString.length() << std::endl;

    // Выводим первый символ строки
    std::cout << "First character: " << myString.front() << std::endl;

    // Выводим последний символ строки
    std::cout << "Last character: " << myString.back() << std::endl;

    return 0;
}
